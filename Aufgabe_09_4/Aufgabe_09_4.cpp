/*** Aufgabe_09_4.cpp ***/
#include "Aufgabe_09_4.h"

using namespace std;

Fraction::Fraction() {

}

Fraction::Fraction(int const numerator, int const denominator) {
    if (denominator == 0) {
        this->numerator = 1;
        this->denominator = 1;
    } else {
        this->numerator = numerator;
        this->denominator = denominator;
    }

    normalize();
}

void Fraction::normalize() {
    if (denominator < 0){
        numerator = -numerator;
        denominator = -denominator;
    }

    int ggt = abs(gcd(numerator, denominator));

    numerator /= ggt;
    denominator /= ggt;
}

int Fraction::gcd(int const n, int const d) {
    if (d == 0)
        return n;
    else
        return gcd(d, n % d);
}

Fraction Fraction::operator+(Fraction const &f) {
    return Fraction(denominator * f.numerator + numerator * f.denominator, denominator * f.denominator);
}

Fraction Fraction::operator-(Fraction const &f) {
    return Fraction(denominator * f.numerator - numerator * f.denominator, denominator * f.denominator);
}

Fraction Fraction::operator*(Fraction const &f) {
    return Fraction(numerator * f.numerator, denominator * f.denominator);
}

Fraction Fraction::operator/(Fraction const &f) {
    return Fraction(numerator * f.denominator, f.numerator * denominator);
}

int Fraction::getNumerator() const {
    return numerator;
}

int Fraction::getDenominator() const {
    return denominator;
}

void Fraction::print() {
    std::cout << *this << endl;
}

ostream& operator<<(ostream &os, Fraction const &f) {
    os << f.numerator << "/" << f.denominator;
    return os;
}

/*** Ende Aufgabe_09_4.cpp ***/
